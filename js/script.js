function dropInfo(file) {
    placeToInsert.classList.remove("inactive");
    placeToInsert.insertAdjacentHTML(
        "afterbegin",
        `<p class="form-description inserted">Завантажено файл <span class="file-name">${file[0].name}</span></p>`
    );
    placeToRemove.classList.add("inactive");
}

const placeToInsert = document.querySelector(".info-wrap");
const placeToRemove = document.querySelector(".download-wrap");
const dropContainer = document.getElementById("drop-container");
const fileInput = document.getElementById("file-input");
const resetBtn = document.getElementById("reset");

// dragover and dragenter events need to have 'preventDefault' called
// in order for the 'drop' event to register.
dropContainer.ondragover = dropContainer.ondragenter = function (e) {
    e.preventDefault();
};

dropContainer.ondrop = function (e) {
    fileInput.files = e.dataTransfer.files;
    dropInfo(fileInput.files);

    e.preventDefault();
};

fileInput.onchange = function () {
    dropInfo(fileInput.files);
};

resetBtn.onclick = function () {
    const insertedParagraph = document.querySelector(".inserted");
    insertedParagraph.remove();
    placeToInsert.classList.add("inactive");
    placeToRemove.classList.remove("inactive");
};
